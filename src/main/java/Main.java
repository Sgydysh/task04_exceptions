public class Main {


    public static class MyResource implements AutoCloseable {

        public void doSomething() {
            throw new RuntimeException("From the doSomething method");
        }


        @Override
        public void close() throws Exception {
            throw new ArithmeticException("I can throw whatever I want, you can't stop me.");
        }
    }

    public static void main(String[] arguments) throws Exception {

        try (MyResource resource = new MyResource()) {
            resource.doSomething();
        }
        catch (Exception e) {
            System.out.println("Regular exception: " + e.toString());


            Throwable[] suppressedExceptions = e.getSuppressed();
            int n = suppressedExceptions.length;

            if (n > 0) {
                System.out.println("We've found " + n + " suppressed exceptions:");
                for (Throwable exception : suppressedExceptions) {
                    System.out.println(exception.toString());
                }
            }
        }
    }


}
